superbuild_add_project(opencascadesession
  DEVELOPER_MODE
  DEBUGGABLE
  DEPENDS boost cxx11 libarchive occt paraview qt5 smtk
  DEPENDS_OPTIONAL python3
  CMAKE_ARGS
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DENABLE_PYTHON_WRAPPING:BOOL=${python3_enabled})

superbuild_declare_paraview_xml_files(opencascadesession
  FILE_NAMES "smtk.opencascadesession.xml"
  DIRECTORY_HINTS "smtk-${smtk_version}")
