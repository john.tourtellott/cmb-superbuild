set(paraview_extra_cmake_args)
if (PV_NIGHTLY_SUFFIX)
  list(APPEND paraview_extra_cmake_args
    -DPV_NIGHTLY_SUFFIX:STRING=${PV_NIGHTLY_SUFFIX})
endif ()

if (UNIX AND NOT APPLE)
  list(APPEND paraview_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

set(paraview_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND paraview_rpaths
    "${qt5_rpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  paraview_rpaths
  "${paraview_rpaths}")

# Ubuntu toolchains don't allow undefined symbols, but VTK's autodetection
# doesn't see this.
if (EXISTS "/etc/os-release")
  file(STRINGS "/etc/os-release" paraview_os_id REGEX "^ID=")
  if (paraview_os_id MATCHES "ubuntu")
    list(APPEND paraview_extra_cmake_args
      -Dvtk_undefined_symbols_allowed:BOOL=OFF)
  endif ()
endif ()

if (matplotlib_enabled)
  set(paraview_matplotlib_selection YES)
else ()
  set(paraview_matplotlib_selection NO)
endif ()

if (gdal_enabled)
  set(paraview_gdal_options
    -DVTK_MODULE_ENABLE_VTK_GeovisCore:STRING=YES
    -DPARAVIEW_ENABLE_GDAL:BOOL=${gdal_enabled}
  )
else()
  set(paraview_gdal_options
    -DVTK_MODULE_ENABLE_VTK_GeovisCore:STRING=NO
    -DPARAVIEW_ENABLE_GDAL:BOOL=${gdal_enabled}
  )
endif()

set(vtk_smp_backend "STDThread")
if (tbb_enabled)
  set(vtk_smp_backend "TBB")
endif ()

set(paraview_dll_paths)
if (qt5_dllpath)
  list(APPEND paraview_dll_paths
    "${qt5_dllpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}" paraview_dll_paths "${paraview_dll_paths}")

if (gdal_enabled)
  # If gdal enabled, turn on geovis to provide the gdal reader
  list(APPEND paraview_extra_cmake_args
    -DVTK_MODULE_ENABLE_VTK_GeovisCore:STRING=YES
    -DPARAVIEW_ENABLE_GDAL:BOOL=ON
  )
else ()
  # Force options back to default to prevent confusion in rebuilds
  # that may have been configured differently previously.
  list(APPEND paraview_extra_cmake_args
    -DVTK_MODULE_ENABLE_VTK_GeovisCore:STRING=DEFAULT
    -DPARAVIEW_ENABLE_GDAL:BOOL=OFF
  )
endif ()

# Disable VTK-m when building ParaView 5.9. There are issues with
# newer xcode when building.
if (APPLE AND paraview_SOURCE_SELECTION STREQUAL "for-slac")
  list(APPEND paraview_extra_cmake_args -DPARAVIEW_USE_VTKM:BOOL=OFF)
endif ()

superbuild_add_project(paraview
  DEBUGGABLE
  DEPENDS
    boost
    freetype
    png
    qt5
    zlib
    netcdf
  DEPENDS_OPTIONAL
    cxx11 hdf5 protobuf las gdal
    python3 tbb
  CMAKE_ARGS
    -DPARAVIEW_BUILD_SHARED_LIBS:BOOL=ON
    -DPARAVIEW_BUILD_TESTING:BOOL=OFF
    -DPARAVIEW_PLUGIN_ENABLE_SLACTools:BOOL=ON
    -DPARAVIEW_PLUGINS_DEFAULT:BOOL=OFF
    -DPARAVIEW_USE_QT:BOOL=${qt5_enabled}
    -DPARAVIEW_USE_PYTHON:BOOL=${python3_enabled}
    -DPARAVIEW_ENABLE_WEB:BOOL=OFF
    -DPARAVIEW_USE_MPI:BOOL=${mpi_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_hdf5:BOOL=${hdf5_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_netcdf:BOOL=${netcdf_enabled}
    -DVTK_MODULE_USE_EXTERNAL_ParaView_protobuf:BOOL=${protobuf_enabled}
    -DPARAVIEW_BUILD_WITH_KITS:BOOL=ON
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib

    # enable VisIt bridge for additional file readers
    -DPARAVIEW_ENABLE_VISITBRIDGE:BOOL=ON

    -DPARAVIEW_UNIFIED_INSTALL_TREE:BOOL=ON
    -DVTK_DLL_PATHS:STRING=${paraview_dll_paths}

    #CMB needs geovis enabled to provide the gdal reader
    ${paraview_gdal_options}
    -DPARAVIEW_ENABLE_LAS:BOOL=${las_enabled}
    -DVTK_MODULE_ENABLE_VTK_DomainsChemistryOpenGL2:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingMatplotlib:STRING=${paraview_matplotlib_selection}
    -DVTK_MODULE_ENABLE_VTK_RenderingGL2PSOpenGL2:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_ViewsInfovis:STRING=YES

    # CMB needs to specify external plugins so that we can let paraview
    # properly install the plugins. So we sneakily force a variable that is an
    # implementation detail of paraview branding
    -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=TRUE
    # since VTK mangles all the following, I wonder if there's any point in
    # making it use system versions.
    -DVTK_MODULE_USE_EXTERNAL_VTK_freetype:BOOL=${freetype_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_png:BOOL=${png_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_zlib:BOOL=${zlib_enabled}

    # Sequential Backend
    -DVTK_SMP_IMPLEMENTATION_TYPE:STRING=${vtk_smp_backend}

    #If this is true paraview doesn't properly clean the paths to system
    #libraries like netcdf
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=TRUE
    -DCMAKE_MACOSX_RPATH:BOOL=FALSE
    -DCMAKE_INSTALL_RPATH:STRING=${paraview_rpaths}

    ${paraview_response_file}

    # Keep the legacy API on (but with warnings) so it's possible to merge
    # in the face of changes that would break dependent external projects.
    -DPARAVIEW_BUILD_LEGACY_REMOVE:BOOL=OFF

    ${paraview_extra_cmake_args})

superbuild_declare_paraview_xml_files(paraview
  FILE_NAMES "paraview.plugins.xml"
  DIRECTORY_HINTS "paraview-${paraview_version}/plugins")

set(paraview_paraview_dir "<INSTALL_DIR>/lib/cmake/paraview-${paraview_version}")

superbuild_add_extra_cmake_args(
  -DParaView_DIR:PATH=${paraview_paraview_dir}
  -DParaView_CLEXECUTABLES_DIR:PATH=<INSTALL_DIR>/bin
  -DVTK_DIR:PATH=${paraview_paraview_dir}/vtk)
