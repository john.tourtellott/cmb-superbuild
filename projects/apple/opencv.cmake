include("${CMAKE_CURRENT_LIST_DIR}/../opencv.cmake")

if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS "8.0.0")
  superbuild_apply_patch(opencv coreimage-videoio-link
    "Remove CoreImage from videoio's linkline")
endif ()
