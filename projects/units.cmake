# Build and install:
superbuild_add_project(units
  DEPENDS cxx11 pegtl nlohmannjson eigen
  CMAKE_ARGS
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=ON
    -Dunits_ENABLE_QT_SUPPORT:BOOL=OFF
    -Dunits_BUILD_TESTS:BOOL=OFF
)
