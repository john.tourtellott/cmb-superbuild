if (superbuild_python_version)
  set(xms_python_version "${superbuild_python_version}")
else ()
  set(xms_python_version "3")
endif ()

superbuild_add_project(xmscore
  DEPENDS boost zlib
  DEPENDS_OPTIONAL pybind11 python3
  CMAKE_ARGS
    -Wno-dev
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DBUILD_TESTING:BOOL=OFF
    -DENABLE_PYTHON:BOOL=${pybind11_enabled}
    -DPYTHON_TARGET_VERSION:STRING=${xms_python_version}
    -DXMS_BUILD_MODE:STRING=CMAKE
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
)
