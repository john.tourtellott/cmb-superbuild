set(libarchive_xml_dep)
set(libarchive_use_libxml2 OFF)
if (NOT WIN32)
  set(libarchive_xml_dep libxml2)
  set(libarchive_use_libxml2 ON)
endif ()

superbuild_add_project(libarchive
  DEPENDS bzip2 zlib xz ${libarchive_xml_dep}
  DEPENDS_OPTIONAL openssl
  CMAKE_ARGS
    -DBUILD_TESTING:BOOL=OFF
    -DBUILD_TEST:BOOL=OFF
    -DENABLE_TEST:BOOL=OFF
    -DENABLE_WERROR:BOOL=OFF
    -DENABLE_LIBB2:BOOL=OFF
    -DENABLE_LZ4:BOOL=OFF
    -DENABLE_LZMA:BOOL=${xz_enabled}
    -DENABLE_ZSTD:BOOL=OFF
    -DENABLE_BZip2:BOOL=${bzip2_enabled}
    -DENABLE_ZLIB:BOOL=${zlib_enabled}
    -DENABLE_LIBXML2:BOOL=${libarchive_use_libxml2}
    -DENABLE_EXPAT:BOOL=OFF
    -DENABLE_OPENSSL:BOOL=${openssl_enabled}
    -DCMAKE_INSTALL_RPATH:PATH=$ORIGIN/../lib
    # quiet "developer-only" warnings
    -Wno-dev
    )

if (UNIX AND NOT APPLE)
  superbuild_add_extra_cmake_args(
    -DLibArchive_INCLUDE_DIR:PATH=<INSTALL_DIR>/include
    -DLibArchive_LIBRARY:FILEPATH=<INSTALL_DIR>/lib/${CMAKE_SHARED_LIBRARY_PREFIX}archive${CMAKE_SHARED_LIBRARY_SUFFIX}
    )
endif()
