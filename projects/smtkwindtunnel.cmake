set(smtkwindtunnel_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND smtkwindtunnel_rpaths
    "${qt5_rpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  smtkwindtunnel_rpaths
  "${smtkwindtunnel_rpaths}")

set(smtkwindtunnel_extra_cmake_options)

set(smtkwindtunnel_enable_by_default OFF)
if ("${SUPERBUILD_PACKAGE_MODE}" STREQUAL "windtunnel")
  set(smtkwindtunnel_enable_by_default ON)
endif()

superbuild_add_project(smtkwindtunnel
  DEBUGGABLE
  DEPENDS boost cxx11 paraview python3 qt5 smtk
  CMAKE_ARGS
    ${smtkwindtunnel_extra_cmake_options}
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_RPATH:STRING=${smtkwindtunnel_rpaths}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DSIMULATION_WORKFLOWS_ROOT:PATH=<INSTALL_DIR>/share/cmb/workflows
    -DENABLE_PLUGIN_BY_DEFAULT=${smtkwindtunnel_enable_by_default}
)

superbuild_declare_paraview_xml_files(smtkwindtunnel
  FILE_NAMES "smtk.windtunnel.xml"
  DIRECTORY_HINTS "smtk-${smtk_version}")
