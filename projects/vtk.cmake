if (vtk_enabled AND NOT (vtkonly_enabled OR paraview_enabled))
  message(FATAL_ERROR
    "VTK is enabled, but neither vtkonly nor paraview has been enabled.")
endif ()

superbuild_add_dummy_project(vtk
  DEPENDS_OPTIONAL vtkonly paraview)
