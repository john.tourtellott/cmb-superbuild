# Upload CMB-ES package files to data.kitware.com.
# Must be run in env with girder_client installed.
# This script is hard-coded to the private CMB-ES folder,
# but can be modified to other girder instances/folders.

# This scipt posts a manifest.json file to the root folder
# to assist with removing "old" releases. The manifest format is
# a list of dictionary. Each element in the list contains:
#
#   name: (string) required: folder name, typically a datecode
#   created: (string) date/time code
#   Linux: (list) linux packages (names) uploaded to this folder
#   macOs: (list) macOS packages (names) uploaded to this folder
#   Windows: (list) Windows packages (names) uploaded to this folder

from __future__ import print_function

import datetime
import glob
import io
import json
import logging
import os
import platform
import sys

import girder_client
print('girder_client version', girder_client.__version__)
from girder_client import GirderClient

girder_host = 'https://data.kitware.com'
api_url = girder_host + '/api/v1'


def find_packages(folder):
  '''Finds package files in specified folder.
  '''
  package_list = list()
  # Grabs all files with "package" extensions
  extensions = ['.dmg', '.msi', '.tar.gz', '.tgz', '.zip']
  for ext in extensions:
    pattern = '%s/*%s' % (source_folder, ext)
    package_list += glob.glob(pattern)
  #print('package_list', package_list)
  return package_list

def upload_manifest(client, manifest_file_id, manifest):
  '''Uploads manifest file to girder

  '''
  manifest_string = json.dumps(manifest, sort_keys=True, indent=2)
  stream = io.BytesIO(manifest_string.encode('ascii'))
  client.uploadFileContents(
    manifest_file_id, stream, len(manifest_string))

def get_manifest_file_item(client):
  '''Returns manifest file item, creating it if needed

  '''
  filename = 'manifest.json'
  item_gen = client.listItem(client.root_folder_id, name=filename)
  manifest_item = next(item_gen, None)
  file_item = None
  if manifest_item is not None:
    # If manifest item is found, get its file item
    logging.debug('manifest_item: %s', manifest_item)
    manifest_item_id = manifest_item['_id']
    file_gen = client.listFile(manifest_item_id)
    file_item = next(file_gen, None)
  else:
    # Upload empty list
    manifest_string = json.dumps([])
    stream = io.BytesIO(manifest_string.encode('ascii'))
    file_item = client.uploadFile(
      client.root_folder_id,
      stream,
      filename,
      len(manifest_string),
      parentType='folder')

  # Get the file item from the manifest item
  logging.debug('file_item: %s', file_item)
  return file_item

def get_manifest(client, manifest_file_id):
  '''Downloads and decodes manifest file

  '''
  logging.debug('manifest_file_id %s', manifest_file_id)
  stream = io.BytesIO()
  client.downloadFile(manifest_file_id, stream)
  manifest_string = stream.getvalue().decode('ascii')
  logging.debug('manifest_string: %s', manifest_string)
  manifest = json.loads(manifest_string)
  return manifest

def get_manifest_element(manifest, folder_name):
  '''Scans the manifest for an item with the specified folder name.

  Creates entry if not found but does NOT upload the change.
  The manifest is a list of dictionary objects. Iterate and look
  for a matching name.
  '''
  for element in manifest:
    if element['folder'] == folder_name:
      return element

  # Create element if not found
  element = dict()
  element['folder'] = folder_name
  system_name = platform.system()
  element[system_name] = []
  dt = datetime.datetime.now()
  element['created'] = dt.isoformat()
  manifest.append(element)
  return element

def update_manifest_element(manifest, folder_name, package_names):
  '''Adds packages names to this platform's list

  '''
  found = False
  for index, element in enumerate(manifest):
    if element['folder'] == folder_name:
      found = True
      break

  if not found:
    raise Exception('manifest missing element %s' % element_name)

  # Check for folder id
  folder_gen = client.listFolder(client.root_folder_id, name=folder_name)
  folder = next(folder_gen, None)
  if folder is None:
    logging.warning('no id found for folder %s', folder_name)
    element['folder_id'] = None
  else:
    element['folder_id'] = folder['_id']

  system_name = platform.system()
  package_list = element.get(system_name, [])
  package_set = set(package_list)
  for name in package_names:
    package_set.add(name)
  package_list = list(package_set)
  package_list.sort()
  element[system_name] = package_list
  manifest[index] = element

def prune_empty_folders(client, manifest):
  '''Removes any empty folders
  '''
  logging.debug('Enter prune_empty_folders')
  folder_gen = client.listFolder(client.root_folder_id)
  for folder in folder_gen:
    folder_id = folder['_id']
    folder_name = folder['name']
    details = client.get('folder/' + folder_id + '/details')
    n_items = details.get('nItems')
    logging.debug('folder %s nItems %d' % (folder_name, n_items))
    if 0 == n_items:
      print('Deleting folder %s' % folder_name)
      try:
        response = client.delete('/folder/' + folder_id)
        print(response['message'])

        # Is item in manifest file?
        for i,element in enumerate(manifest):
          if element['folder_id'] == folder_id:
            print('Deleting %s from manifest' % folder_name)
            del manifest[i]
      except Exception as ex:
        print('Exception:', ex)
        continue


def prune_archives(client, manifest, archive_length):
  '''Deletes old releases

  This was added to fix empty folders that sometimes get left
  behind. (Reason for that is not currently known.)
  '''
  system_name = platform.system()
  # Traverse manifest to find which folders have releases for this target.
  # Create a list of indexs into the manifest
  index_list = list()
  for index,element in enumerate(manifest):
    if system_name in element:
      platform_list = element.get(system_name)
      if not platform_list:
        continue

      index_list.append(index)

  # If length less than specified max, we are ok
  if len(index_list) <= archive_length:
    return

  # Manifest is sorted, so strip off the last N entries
  index_list = index_list[:-archive_length]
  logging.debug('index_list %s' % index_list)

  for index in index_list:
    element = manifest[index]
    package_folder_id = element.get('folder_id')
    # Check if folder exists
    try:
      package_folder = client.getFolder(package_folder_id)
    except Exception as ex:
      print('EXCEPTION:', ex)
      del manifest[index]
      continue

    package_list = element.get(system_name)
    for package in package_list:
      package_gen = client.listItem(package_folder_id, name=package)
      package_item = next(package_gen, None)
      if package_item:
        package_item_id = package_item['_id']
        response = client.delete('/item/' + package_item_id)
        print(response['message'])
    del manifest[index][system_name]

    # Is package folder now empty?
    details = client.get('folder/' + package_folder_id + '/details')
    n_items = details.get('nItems')
    if 0 == n_items:
      try:
        response = client.delete('/folder/' + package_folder_id)
        print(response['message'])
        del manifest[index]
      except Exception as ex:
        print('EXCEPTION:', ex)
        continue


if __name__ == '__main__':
  '''
  '''
  import argparse
  parser = argparse.ArgumentParser(
    description='Uploads package files to data.kitware.com')

  parser.add_argument('api_key', help='(Required) girder user api key',)
  parser.add_argument('root_folder_id',
    help='(Required) ID of the root folder. The target folder is added to this.')

  parser.add_argument('-a', '--archive_length', type=int, default=3,
    help='max number of releases to keep [3]')
  parser.add_argument('-d', '--dry_run', action='store_true',
    help='creates target folder but does not upload files')
  parser.add_argument('-l', '--logging_debug', action='store_true',
    help='set logging severity level to debug')
  parser.add_argument('-s', '--source_folder', default=None,
    help='folder path on local machine [working directory]')
  parser.add_argument('-t', '--target_folder',
    help='folder name on target machine [current datecode]')
  parser.add_argument('-n', '--no_overwrite_files', action='store_true',
    help='do NOT overwrite existing files')
  args = parser.parse_args()

  if args.logging_debug:
    logging.basicConfig(level=logging.DEBUG)

  # Start girder client
  client = GirderClient(apiUrl=api_url)
  client.authenticate(apiKey=args.api_key)

  client.root_folder_id = args.root_folder_id

  # Make sure root folder exists
  root_folder = client.getFolder(client.root_folder_id)
  if not root_folder:
    raise Exception('Root folder not found')
  print('root_folder:', root_folder['name'])

  # Get the current manifest
  manifest_file_item = get_manifest_file_item(client)
  manifest_file_id = manifest_file_item['_id']
  manifest = get_manifest(client, manifest_file_id)
  logging.debug('initial manifest: %s', manifest)

  # Prune current folders
  if not args.dry_run:
    # Remove old releases
    prune_archives(client, manifest, args.archive_length)
    logging.debug('after prune archives: %s', manifest)

    # Remove any empty folders
    prune_empty_folders(client, manifest)
    logging.debug('after prune empty folders: %s', manifest)

    upload_manifest(client, manifest_file_id, manifest)

  # Get package files
  source_folder = args.source_folder
  if source_folder is None:
    source_folder = os.getcwd()
  print('Source folder:', source_folder)
  package_list = find_packages(source_folder)


  # If no packages found, stop here
  if not package_list:
    print('No packages found in ', os.path.abspath(source_folder))
    print('Done')
    sys.exit(0)


  # Get or create name of package folder on target machine
  package_folder_name = args.target_folder
  if package_folder_name is None:
    # Use current datecode
    dt = datetime.datetime.now()
    package_folder_name = dt.strftime('%Y-%m-%d')
    print('package_folder_name', package_folder_name)

  # Get target folder, create if needed
  package_folder_gen = client.listFolder(client.root_folder_id, name=package_folder_name)
  package_folder = next(package_folder_gen, None)
  if package_folder is None:
    print('Creating folder', package_folder_name)
    package_folder = client.createFolder(client.root_folder_id, package_folder_name)
  package_folder_id = package_folder['_id']
  print('package_folder_id', package_folder_id)

  upload_names = list()  # list of packages that were uploaded

  # If OK to overwrite, just do it
  if not args.no_overwrite_files:
    for package in package_list:
      package_name = os.path.basename(package)
      print('Uploading', package_name)
      client.upload(package, package_folder_id,
        reuseExisting=True, dryRun=args.dry_run)
      upload_names.append(package_name)

  # Otherwise get list of current files in the package folder
  else:
    current_filenames = set()
    girder_files = client.listItem(package_folder_id)
    for f in girder_files:
      #print('f', f)
      name = f['name']
      current_filenames.add(name)
    print('current_filenames', current_filenames)

    for package in package_list:
      package_name = os.path.basename(package)
      if package_name in current_filenames:
        print('INFO: NOT overwriting', package_name)
        continue

      print('Uploading', package_name)
      client.upload(package, package_folder_id,
        reuseExisting=True, dryRun=args.dry_run)
      upload_names.append(package_name)

  print()
  prefix = '(DRY RUN)' if args.dry_run else ''
  print(prefix, 'Number of packages uploaded:', len(upload_names))

  # Update manifest
  element = get_manifest_element(manifest, package_folder_name)
  update_manifest_element(manifest, package_folder_name, upload_names)
  logging.debug('after update manifest: %s', manifest)
  if not args.dry_run:
    upload_manifest(client, manifest_file_id, manifest)
  print(prefix, 'Uploaded manifest.json file')

  sys.exit(0)
