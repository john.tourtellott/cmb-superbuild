# Rebuilding CI images manually

Rebuilding the images as the superbuild does (with `sccache` and all) can be
done by following these steps (may need tweaking for your local machine):

```sh
cd path/to/cmb-superbuild/source
git checkout $commit_to_add_to_ci_images
# Make sccache available.
.gitlab/ci/sccache.sh
export SCCACHE_REDIS=redis://redis.kitware.com

# Emulate the gitlab-ci environment. May need tweaking to emulate other
# branches or MR builds.
export CI_PROJECT_PATH=cmb/cmb-superbuild
export CI_COMMIT_REF_NAME=master
export CI_COMMIT_SHA=$( git rev-parse HEAD )

# Add credentials for pushing the resulting images.
export DOCKERHUB_USERNAME=username-that-can-upload-to-kitware/cmb
export DOCKERHUB_PASSWORD=password-or-app-token

# Comment out the `dnf install` line in the script and ensure that `podman` is
# installed locally.

# Build SMTK images.
for x in {"",-{paraview,vtk}}; do
    .gitlab/ci/build-smtk-image.sh fedora31$x fedora31$x
done
# Build the CMB image.
.gitlab/ci/build-cmb-image.sh fedora31 fedora31
```
