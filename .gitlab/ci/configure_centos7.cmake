set(ENABLE_python3 ON CACHE BOOL "")

# Qt 5.15 is required by SMTK's qtOntologyItem for two things:
# 1. The QComboBox::textActivated signal (5.14+) and
# 2. Its .ui file contains a QTextArea that includes a "markdown"
#    property. If Qt 5.15 is used to save the file, this will
#    autogenerate code that breaks builds using 5.12.
set(qt5_SOURCE_SELECTION "5.15" CACHE STRING "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_common.cmake")
