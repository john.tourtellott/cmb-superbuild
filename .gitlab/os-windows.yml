# Windows-specific builder configurations and build commands

## Base configurations

.windows:
    variables:
        GIT_SUBMODULE_STRATEGY: none
        GIT_CLONE_PATH: "$CI_BUILDS_DIR\\cmb-ci-ext\\$CI_CONCURRENT_ID"
        # Avoid conflicting with other projects running on the same machine.
        SCCACHE_SERVER_PORT: 4228
        # Turn off direct mode until we test it.
        BUILDCACHE_DIRECT_MODE: "false"

### Build and test

.windows_build:
    extends: .windows

    variables:
        # Note that shell runners only support runners with a single
        # concurrency level. We can't use `$CI_CONCURRENCY_ID` because this may
        # change between the build and test stages which CMake doesn't support.
        # Even if we could, it could change if other runners on the machine
        # could run at the same time, so we drop it.
        GIT_CLONE_PATH: "$CI_BUILDS_DIR\\cmb-ci"
        # Force "desktop" OpenGL support. Qt seems to want to use EGL when run
        # from GitLab-CI by default (it runs as a Windows service).
        QT_OPENGL: desktop

.windows_vs2022:
    extends: .windows_build

    variables:
        VCVARSALL: "${VS170COMNTOOLS}\\..\\..\\VC\\Auxiliary\\Build\\vcvarsall.bat"
        VCVARSPLATFORM: "x64"
        VCVARSVERSION: "14.32.31326"

.windows_vs2019_1426:
    extends: .windows_build

    variables:
        VCVARSALL: "${VS160COMNTOOLS}\\..\\..\\VC\\Auxiliary\\Build\\vcvarsall.bat"
        VCVARSPLATFORM: "x64"
        # ITK's internal OpenJPEG fails with 14.28.29910 at least.
        VCVARSVERSION: "14.26"

.windows_vs2019_aeva:
    extends: .windows_vs2019_1426

    variables:
        CMAKE_CONFIGURATION: windows_vs2019_aeva

.windows_vs2022_windtunnel:
    extends: .windows_vs2022

    variables:
        CMAKE_CONFIGURATION: windows_vs2022_windtunnel

.windows_vs2022_truchas:
    extends: .windows_vs2022

    variables:
        CMAKE_CONFIGURATION: windows_vs2022_truchas

.windows_vs2022_ace3p:
    extends: .windows_vs2022

    variables:
        CMAKE_CONFIGURATION: windows_vs2022_ace3p

.windows_vs2022_modelbuilder:
    extends: .windows_vs2022

    variables:
        CMAKE_CONFIGURATION: windows_vs2022_modelbuilder

.windows_vs2022_smtk:
    extends: .windows_vs2022

    variables:
        CMAKE_CONFIGURATION: windows_vs2022_smtk

## Tags

.windows_builder_tags:
    tags:
        - cmb # Since this is a bare runner, pin to a project.
        - msvc-19.32
        - nonconcurrent
        - shell
        - vs2022
        - windows-x86_64

.windows_builder_tags_aeva:
    tags:
        - cmb # Since this is a bare runner, pin to a project.
        - msvc-19.26
        - nonconcurrent
        - shell
        - vs2019
        - windows-x86_64

## Windows-specific scripts

.before_script_windows: &before_script_windows
    - $pwdpath = $pwd.Path
    - powershell -File ".gitlab/ci/cmake.ps1"
    - Set-Item -Force -Path "env:PATH" -Value "$pwdpath\.gitlab\cmake\bin;$env:PATH"
    - powershell -File ".gitlab/ci/ninja.ps1"
    - Set-Item -Force -Path "env:PATH" -Value "$pwdpath\.gitlab;$env:PATH"
    - cmake --version
    - ninja --version
    - cmake -P .gitlab/ci/download_qt.cmake
    - Set-Item -Force -Path "env:PATH" -Value "$pwdpath\.gitlab\qt\bin;$env:PATH"
    # Add the outputs to the PATH
    - Set-Item -Force -Path "env:PATH" -Value "$env:PATH;$pwdpath\build\install\bin;$pwdpath\build\install\Python"
    - Set-Item -Force -Path "env:PYTHONHOME" -Value "$pwdpath\build\install\Python"
    # Support submodule references to the user's fork.
    - git submodule foreach --recursive cmake -P "$pwdpath/.gitlab/ci/fetch_submodule.cmake"
    - git submodule sync --recursive
    - git submodule update --init --recursive

.cmake_build_windows:
    stage: build

    script:
        - *before_script_windows
        - Set-Item -Force -Path "env:PATH" -Value "$env:PATH;$env:SCCACHE_PATH"
        - Invoke-Expression -Command .gitlab/ci/buildcache.ps1
        - Set-Item -Force -Path "env:PATH" -Value "$env:PATH;$pwdpath\.gitlab\buildcache\bin"
        - Invoke-Expression -Command .gitlab/ci/vcvarsall.ps1
        - buildcache --show-stats
        - ctest -VV -S .gitlab/ci/ctest_configure.cmake
        - ctest -VV -S .gitlab/ci/ctest_build.cmake
        - buildcache --show-stats
    interruptible: true

.cmake_test_windows:
    stage: test

    script:
        - *before_script_windows
        - Invoke-Expression -Command .gitlab/ci/vcvarsall.ps1
        - ctest --output-on-failure -V -S .gitlab/ci/ctest_test.cmake
    interruptible: true
